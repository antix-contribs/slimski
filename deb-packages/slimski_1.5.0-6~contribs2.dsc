Format: 3.0 (quilt)
Source: slimski
Binary: slimski
Architecture: any
Version: 1.5.0-6~contribs2
Maintainer: skidoo <email@redact.ed>
Homepage: https://gitlab.com/skidoo/slimski
Standards-Version: 3.9.8
Build-Depends: debconf, debhelper (>= 9.20151219), dh-exec, po-debconf, sharutils, cmake, libpam0g-dev, libjpeg-dev, libpng-dev, libxft-dev, libxmu-dev
Package-List:
 slimski deb x11 optional arch=any
Checksums-Sha1:
 cd025accc3b13f9f643389c2fe351a0be9dabad7 33852961 slimski_1.5.0.orig.tar.gz
 7c517f8a3e5bb50bf6298c5156917822ea451707 22252 slimski_1.5.0-6~contribs2.debian.tar.xz
Checksums-Sha256:
 59637be5aa38e368f87a20b47deaca6ad1c71fe52450891716f01a1e615b5383 33852961 slimski_1.5.0.orig.tar.gz
 5c6d0909027e57c7fca382fcef7dd90d6bb8fb77ecd67a8604c8cbbac5a90a7c 22252 slimski_1.5.0-6~contribs2.debian.tar.xz
Files:
 497ba274417898efd4cef054a2a6cee1 33852961 slimski_1.5.0.orig.tar.gz
 27cf42873eb539666a1a2c73458a3dd5 22252 slimski_1.5.0-6~contribs2.debian.tar.xz
